jQuery.exists = function (selector) { // функция проверки существования селектора
    return ($(selector).length > 0);
};



(function() {
  $(document).ready(function() {
      $(function() {

          if($.exists('.speed-select')){
              $( ".speed-select" ).selectmenu();
          }

          if($.exists('.speed-select2')){
              $( ".speed-select2" ).selectmenu();
          }

          if($.exists('.bxslider')){
              $('.bxslider').bxSlider({
                  pager: false
              });
          }

          if($.exists('.phone-mask')){
              $(".phone-mask").mask("+7 (999) 999-9999");
          }

          if($.exists('#accordion')){
              $( "#accordion" ).accordion({
                  heightStyle: "content"
              });
          }

          if($.exists('.item-textarea')){
              $(".item-textarea").focus(function() {
                  $(".item-form").children('div').addClass('item-form-focus');
                  $(".item-form-focus-hide").css({display:'block'});
                  $(".item-info-text").css({marginTop: "29px"});
              });


              $(document).click(function(event) {
                  if ($(event.target).closest(".item-form").length) return;
                  $(".item-form").children('div').removeClass('item-form-focus');
                  $(".item-form-focus-hide").css({display:'none'});
                  $(".item-info-text").css({marginTop: "25px"});
                  event.stopPropagation();
              });
          }

          if($.exists('#select-quantity')){
              $('#select-quantity').each(function(){
                  var $this = $(this), numberOfOptions = $(this).children('option').length;

                  $this.addClass('select-hidden');
                  $this.wrap('<div class="select"></div>');
                  $this.after('<div class="select-styled"></div>');

                  var $styledSelect = $this.next('div.select-styled');
                  $styledSelect.text($this.children('option').eq(0).text());

                  var $list = $('<ul />', {
                      'class': 'select-options'
                  }).insertAfter($styledSelect);

                  for (var i = 0; i < numberOfOptions; i++) {
                      $('<li />', {
                          text: $this.children('option').eq(i).text(),
                          rel: $this.children('option').eq(i).val()
                      }).appendTo($list);
                  }

                  var $listItems = $list.children('li');

                  $styledSelect.click(function(e) {
                      e.stopPropagation();
                      $('div.select-styled.active').each(function(){
                          $(this).removeClass('active').next('ul.select-options').hide();
                      });
                      $(this).toggleClass('active').next('ul.select-options').toggle();
                  });

                  $listItems.click(function(e) {
                      e.stopPropagation();
                      $styledSelect.text($(this).text()).removeClass('active');
                      $this.val($(this).attr('rel'));
                      $list.hide();
                      //console.log($this.val());
                  });

                  $(document).click(function() {
                      $styledSelect.removeClass('active');
                      $list.hide();
                  });

              });
          }



          if($.exists(".open-modal")) {
              $('.open-modal').click(function (e) {
                  e.preventDefault();
                  var href = $(this).attr('href');

                  if ($(href).hasClass('active-modal')) {
                      $(href).removeClass('active-modal');
                      $("html,body").css("overflow","auto");
                  }else{
                      $(href).addClass('active-modal');
                      $("html,body").css("overflow","hidden");
                  }


              });
          }




          if($.exists('.form')){
              $('.form').on('submit', function (event) {

                  var form = $(this);
                  var phoneCheck = form.find('.phone-check');
                  var textareaCheck = form.find('.textarea-check');

                  if (phoneCheck.val().length < 2) {
                      $(phoneCheck).addClass('check-error');
                      event.preventDefault();
                  }


                  if (textareaCheck.val().length < 2) {
                      $(textareaCheck).addClass('check-error');
                      event.preventDefault();
                  }
              });
              $('.phone-check, .textarea-check').focus(function() {
                  $(this).removeClass('check-error');
              });
          }











          $('.ui-selectmenu-menu.ui-front:eq(0)').addClass('first-select');

      });
  });
}).call(this);
